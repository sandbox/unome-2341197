
-- SUMMARY --

The DaData module allows to use the DaData suggestions with names,
addresses, information about organization when entering data using text
fields on your site.

For more information about suggestions, visit the project page:
  https://dadata.ru/suggestions/

The module implements Form API element types, Field API field types and
widgets for text fields with support of DaData suggestions.

-- SUMMARY [RU] --

Модуль DaData позволяет использовать Подсказки сервиса автоматической
стандартизации контактных данных DaData.ru при вводе данных на вашем сайте.

Подсказки используются при вводе Ф.И.О., адреса, реквизитов организации.
Для более полной информации о возможностях Подсказок посетите сайт сервиса:
  https://dadata.ru/suggestions/

В модуле реализованы типы элементов для использования в формах (Form API),
специфичные поля и виджеты для текстовых полей (Field API).


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* The DaData requires that you set up the service API key in Administration »
  Configuration » User interface » DaData settings.
  Register on the DaData.ru and generate key in your profile:
    https://dadata.ru/profile/

* Для работы Подсказок после инсталяции необходимо задать API key в настройках
  модуля на Administration » Configuration » User interface » DaData settings.
  API key вы можете посмотреть в Личном кабинете на сайте сервиса
  (https://dadata.ru/profile/) после регистрации.


-- CONFIGURATION --

* Customize the module settings in Administration » Configuration »
  User interface » DaData settings.


-- CUSTOMIZATION --

* To set up custom parameters to DaData jquery.suggestions plugin,
  used by DaData elements, you may use #dadata_params attribute of element:

    $form['address'] = array(
      '#type' => 'dadata_address_comp',
      '#title' => t('Address'),
      '#dadata_params' => array(
        'count' => 5,
        'constraints' => array(
          'label' => 'Бердск (НСО)',
          'restrictions' => array(
            'region' =>  'Новосибирская',
            'city' => 'Бердск',
          ),
          'deletable' => FALSE,
        ),
        'restrict_value' => TRUE,
      ),
    );

* To theme dropdown list with suggestions, you may set up styles for added by
  jquery.suggestions plugin additional markup:

  <input type="text" id="suggestions" class="suggestions-input" />
  <div class="suggestions-wrapper">
      <div class="suggestions-suggestions">
          <div class="suggestions-hint">- Select -</div>
          <div class="suggestions-suggestion suggestions-selected">...</div>
          <div class="suggestions-suggestion">...</div>
          <div class="suggestions-suggestion">...</div>
      </div>
      <i class="suggestions-preloader"></i>
      <ul class="suggestions-constraints"></ul>
  </div>

  .suggestions-suggestions .suggestions-selected { font-weight: bold; }


-- TROUBLESHOOTING --

* If the dropdown list with suggestions does not display, check the following:

  - Is your API key specified in DaData settings right?

  - Didn't you exceed the limit of free suggestions per day?

  - Is service available?


-- CONTACT --

Current maintainers:
* Den Medyantsev (unome) - http://drupal.org/user/2988505

